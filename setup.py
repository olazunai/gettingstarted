from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Simple tutorial for cookiecutter',
    author='Bosonit',
    license='',
)
